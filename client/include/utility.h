#ifndef _UTILITY_H_
#define _UTILITY_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "../include/structs.h"
#include "../include/defines.h"


/// --- function for deallocating memory
void clean(thread_data* td, int threadc) ;

/// --- printing out gathered data
void print_results(data_to_print* data) ;

/// --- function printing information about the program
void help() ;


/// --- function adding hosts to thread_data array (utilized later by created threads),
///     accepts as arguments: current dynamically allocated thread_data array, pointers to variables storing its
///     current and max size, pointer to string which contains information about a host,
///     index of user's choice for the query, keyword searched
///     function basically transforms one string w.x.y.z:abcd to two seperate strings
thread_data* str2thread_data(thread_data* td, int* size, int* maxsize, char* hoststring, int choice, char* keyword) ;
#endif