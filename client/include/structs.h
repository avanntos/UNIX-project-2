#ifndef _STRUCTS_H_
#define _STRUCTS_H_


/// --- structure containing record we search for
typedef struct record {
    int client_id;
    long phone_nr;
    char name[128];
    char address[128];
} record;

/// --- structure we send over the network as a query
typedef struct query_data {
    int type;
    char keyword[64];
} query_data;

/// --- structure containing data ready to print on the screen
typedef struct data_to_print {
    record* rec;
    char* parent_server;
    char* port;
    struct data_to_print* next;
} data_to_print;

/// --- structure containing all information thread needs to perform the query
typedef struct thread_data {
    char* keyword;
    char* server;
    char* port;
    int choice;
    data_to_print* data;
} thread_data;


#endif