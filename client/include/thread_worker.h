#ifndef _THREADW_H_
#define _THREADW_H_

#include <stdlib.h>
#include <stdio.h>
#include <sched.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>


#include "../include/structs.h"
#include "../include/defines.h"

/// --- function handling network io, connection, sending the query,
///     retrieving data and preparing chained list with results
data_to_print* query_server(char* address, char* port, const char* keyword, int choice) ;


/// --- implementation of the thread
void* thread_worker(void* t_data) ;

#endif