#include "../include/utility.h"

void clean(thread_data* td, int threadc) {
    for(int i=0; i<threadc; ++i) {
        free((td + i)->server);
        free((td + i)->port);

        data_to_print* tmp = (td + i)->data;
        while(tmp != NULL) {
            data_to_print* next_to_delete = tmp->next;
            free(tmp->rec);
            free(tmp);
            tmp = next_to_delete;
        }
    }
}

void print_results(data_to_print* data) {
    data_to_print* tmp = data;

    while(tmp != NULL) {
        printf("[%s:%s] %d | %ld | %s | %s\n", tmp->parent_server, tmp->port, tmp->rec->client_id, tmp->rec->phone_nr, tmp->rec->name, tmp->rec->address);
        tmp = tmp->next;
    }
}

void help() {
    printf("Usage:  dbsearch <search type> <search phrase> [address:port]\n");
    printf("arguments enclosed in <> are mandatory, and [] are optional\n");
    printf("Example \tdbsearch 3 adam 12.37.45.68:1337\n");
    printf("supported search types are:\n\t1 - database id\n\t2 - phone number\n\t3 - name\n\t4 - address\n");
    printf("if no additional addresses are provided, hosts from file 'hosts.cfg' will be used\n");
}

thread_data* str2thread_data(thread_data* td, int* size, int* maxsize, char* hoststring, int choice, char* keyword) {
    int l = strlen(hoststring);
    int semic = 0;
    /// finding ':' in the string
    for(int i=0; i<l; ++i)
        if(hoststring[i] == ':')
            semic = i;
        
    /// reallocating if necessary for more space
    if(*size == *maxsize) {
        thread_data* tmp = realloc(td, 2*(*maxsize)*sizeof(thread_data));
        if(tmp == NULL) {
            perror("error acquiring memory");
            exit(-1);
        }
        td = tmp;
        *(maxsize) *= 2;
    }

    /// preparing data for threads
    (td + *(size))->server = malloc(l*sizeof(char));
    (td + *(size))->choice = choice;
    strncpy((td + *(size))->server, hoststring, semic);
    (td + *(size))->port = malloc(strlen(hoststring + l + 1)*sizeof(char));
    strncpy((td + *(size))->port, hoststring + semic + 1, l - semic);
    (td + *(size))->keyword = keyword;

    ++*(size);

    return td;
}