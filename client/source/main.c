#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <fcntl.h>
#include <sched.h>
#include <pthread.h>

#include "../include/defines.h"
#include "../include/structs.h"
#include "../include/utility.h"
#include "../include/thread_worker.h"

int main(int argc, char** argv) {
    if((argc ==1) || (argc == 2)) { /// not enough arguments
        help();
        return -1;
    } else if(argc > 4) {           /// too many arguments
        printf("too many arguments!\n");
        help();
        return -1;
    }

    // int choice = 3;
    // char* keyword = "r";
    int choice = atoi(*(argv + 1));
    if(choice>4 || choice<1) {
        printf("wrong type, supported types are in range 1-4");
        return -1;
    }
    char* keyword = *(argv + 2);

    int hostcount = 0;
    int hostmaxcount = 1;
    thread_data* tdata = malloc(sizeof(thread_data));

    /// opening file with hosts and adding them to thread data
    char buffer[200];
    FILE* config = fopen("hosts.cfg", "r");
    if(config == NULL) {
        perror("cannot open hosts config");
        fclose(config);
    } else {
        while(fscanf(config, "%s", buffer) > 0) {
            tdata = str2thread_data(tdata, &hostcount, &hostmaxcount, buffer, choice, keyword);
        }
        fclose(config);
    }

    /// if host is supplied, adding it to thread data array
    if(argc == 4)
        tdata = str2thread_data(tdata, &hostcount, &hostmaxcount, *(argv + 3), choice, keyword);

    /// create threads array
    pthread_t* threads = malloc(hostcount*sizeof(pthread_t));

    /// start threads
    for(int i=0; i<hostcount; ++i) {
        pthread_create(threads + i, NULL, thread_worker, tdata + i);
    }

    /// join each thread until it finishes its job
    for(int i=0; i<hostcount; ++i) {
        pthread_join(*(threads + i), NULL);
    }

    /// printing results
    for(int i=0; i<hostcount; ++i) {
        print_results((tdata + i)->data);
    }

    /// cleanup
    clean(tdata, hostcount);
    free(threads);
    free(tdata);

    return 0;
}