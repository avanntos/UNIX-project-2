#include "../include/thread_worker.h"

data_to_print* query_server(char* address, char* port, const char* keyword, int choice) {

    /// initializing connection
    struct addrinfo addr;
    memset(&addr, 0, sizeof(addr));
    addr.ai_flags = AI_PASSIVE;
    addr.ai_family = AF_INET;
    addr.ai_socktype = SOCK_STREAM;
    struct addrinfo* servinfo;
    int status = getaddrinfo(address, port, &addr, &servinfo);
    if(status != 0) {
        perror("wrong getaddrinfo() status");
        return(NULL);
    }


    int socketfd = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);
    if(socketfd < 0) {
        perror("socket error");
        return(NULL);
    }

    if(connect(socketfd, servinfo->ai_addr, servinfo->ai_addrlen) == -1) {
        // perror("connection failure");
        return(NULL);
    }

    /// preparing structure that will be sent over the network
    query_data data;
    const char* searchphrase = keyword;
    strncpy(data.keyword, searchphrase, strlen(searchphrase));
    data.type = choice;

    /// preparing message header
    struct msghdr request;
    bzero(&request, sizeof(request));
    
    /// "packing" data to the header
    struct iovec vec;
    vec.iov_base = &data;
    vec.iov_len = sizeof(data);
    request.msg_iov = &vec;
    request.msg_iovlen = 1;

    /// sending the message
    int send_status = sendmsg(socketfd, &request, 0);
    if(send_status == -1) {
        perror("sending message failure");
        return(NULL);        
    }

    /// yielding processor time since that will take a while
    sched_yield();


    data_to_print* ret = NULL;
    int finished = 0;
    
    /// header for received structure
    struct msghdr received;
    bzero(&received, sizeof(received));
    /// buffer for data, space for 10 objects but it should be less than BATCHSIZE declared in server source code
    struct iovec rvec;
    record buffer[BUFFERSIZE];
    bzero(buffer, BUFFERSIZE*sizeof(record));
    rvec.iov_base = buffer;
    rvec.iov_len = BUFFERSIZE*sizeof(record);
    received.msg_iov = &rvec;
    received.msg_iovlen = 1;

    while(finished != 1) {
        /// attempting to retrieve message
        int receive_status = recvmsg(socketfd, &received, MSG_WAITALL);
        if(receive_status == -1) {
            printf("err receiving");
            exit(-1);
        }

        for(int i=0; i<BUFFERSIZE; ++i) {
            /// protocol implemented: getting zeroed record is equal to the end of data stream
            if(buffer[i].client_id == 0 && buffer[i].phone_nr == 0 && buffer[i].name[0] == '\0' && buffer[i].address[0] == '\0') {
                finished = 1;
                break;
            } else {
                /// copying retrieved data from the buffer to data_to_print structure
                data_to_print* new = malloc(sizeof(data_to_print));
                new->parent_server = address;
                new->port = port;
                new->rec = malloc(sizeof(record));
                memcpy(new->rec, buffer + i, sizeof(record));

                /// adding it to the list of retrieved objects
                new->next = ret;
                ret = new;
            }
            // printf("%d %ld %s %s\n", buffer[i].client_id, buffer[i].phone_nr, buffer[i].name, buffer[i].address);
        }
        bzero(buffer, sizeof(buffer));
    }
    free(servinfo);
    return ret;
}

void* thread_worker(void* t_data) {
    thread_data* td = t_data;

    /// querying server
    data_to_print* ret = query_server(td->server, td->port, td->keyword, td->choice);

    /// nothing found, exiting
    if(ret == NULL)
        pthread_exit(NULL);

    /// adding found data to the linked list structure
    data_to_print* tmp = td->data;

    if(tmp == NULL) {
        td->data = ret;
    } else {
        while(tmp->next != NULL) {
            tmp = tmp->next;
        }

        tmp->next = ret;
    }

    pthread_exit(NULL);

}