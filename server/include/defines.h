#ifndef _DEFINES_H_
#define _DEFINES_H_

/// --- default port if user doesn't specify his own
#define PORT "9998"

/// --- records sent int batches of 5
#define BATCHSIZE 5

/// --- simpler function pointers
typedef int (*searchtype)(record*,query_data*);

#endif