#ifndef _STRUCTS_H_
#define _STRUCTS_H_

/// --- structure containing record we search for
typedef struct record {
    int client_id;
    long phone_nr;
    char name[128];
    char address[128];
} record;

/// --- structure we send over the network as a query
typedef struct query_data {
    int type;
    char keyword[64];
} query_data;

/// --- structure containing data about the client, needed to be processed by worker thread
typedef struct client_data {
    int client_socket_fd;
    query_data* qd;
    struct client_data* next;
} client_data;

/// --- data necessary for thread operations such as memory mapped database file
typedef struct thread_data {
    record* records;
    int n_records;
    client_data* cd;
    pthread_mutex_t* mtx;
} thread_data;

#endif