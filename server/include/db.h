#ifndef _DATAB_H_
#define _DATAB_H_

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <limits.h>
#include <errno.h>

#include "../include/structs.h"
#include "../include/defines.h"

/// --- adding index of found record to array of record indexes
void add_index(int** data, int* size, int* size_max, int value) ;

/// --- 4 functions for little bit simpler searching implementation, one for each type of data searched
int search_id(record* rec, query_data* data) ;
int search_phone(record* rec, query_data* data) ;
int search_name(record* rec, query_data* data) ;
int search_address(record* rec, query_data* data) ;


/// --- function doing actual database lookup, as arguments accepts array of records from the memory mapped file,
///     number of records in said file, data regarding the query and pointer to variable where amount of records found will be stored
struct iovec* db_lookup(record* rec, int db_size, query_data* data, int* size) ;

#endif