#ifndef _THREADW_H_
#define _THREADW_H_

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sched.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#include "../include/db.h"
#include "../include/structs.h"
#include "../include/defines.h"

/// main thread serving requests
void* thread_worker(void*) ;

#endif