#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <fcntl.h>
#include <limits.h>
#include <errno.h>
#include <sched.h>
#include <pthread.h>

#include "../include/structs.h"
#include "../include/defines.h"
#include "../include/db.h"
#include "../include/thread_worker.h"

/// --- handling signal
void ctrlc(int c) {
    exit(0);
}

void start(char* port) {
    /// mapping database file in memory using readonly mode
    const char db_filename[] = "db1.txt";

    struct stat st;
    if( stat(db_filename, &st) != 0) {
        perror("stat error");
        exit(-1);
    }

    int fd = open(db_filename, O_RDONLY);
    if(fd < 0) {
        perror("open() error");
        exit(-1);
    }

    int n_rec = st.st_size/sizeof(record);
    // record* records = mmap(0, 10*sizeof(record), PROT_READ, MAP_PRIVATE, fd, 0);
    record* records = mmap(0, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0);

    if(records == MAP_FAILED) {
        perror("map failed");
        exit(-1);
    }

    /// initializing socket and starting to listen
    struct addrinfo addr;
    memset(&addr, 0, sizeof(addr));
    addr.ai_flags = AI_PASSIVE;
    addr.ai_family = AF_UNSPEC;
    addr.ai_socktype = SOCK_STREAM;
    struct addrinfo* servinfo;
    int status = getaddrinfo(0, port, &addr, &servinfo);
    if(status != 0) {
        perror("wrong getaddrinfo() status");
        exit(-1);
    }

    int socketfd = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);
    if(socketfd < 0) {
        perror("no socket");
        exit(-1);
    }

    if(bind(socketfd, servinfo->ai_addr, servinfo->ai_addrlen) == -1) {
        perror("bind error");
        exit(-1);
    }

    if(listen(socketfd, (100>SOMAXCONN ? SOMAXCONN : 100)) == -1) {
        perror("listen error");
        exit(-1);
    }


    struct sockaddr client_info;
    unsigned int client_addrlen = sizeof(client_info);
    
    /// preparing data necessary for worker thread
    thread_data* td = malloc(sizeof(thread_data));
    td->records = records;
    td->n_records = n_rec;
    td->cd = NULL;
    pthread_mutex_t mt = PTHREAD_MUTEX_INITIALIZER;
    td->mtx = &mt;
    pthread_t worker;
    /// starting thread
    pthread_create(&worker, NULL, thread_worker, td);
    while(1) {
        /// attempting to accept connection
        int newsockfd = accept(socketfd, &client_info, &client_addrlen);

        if(newsockfd >= 0) {
            printf("client accepted\n");

            /// preparing buffer for client query
            struct query_data buffer;
            struct iovec vec;
            vec.iov_base = &buffer;
            vec.iov_len = sizeof(query_data);

            /// preparing message header
            struct msghdr receive_data;
            bzero(&receive_data, sizeof(receive_data));
            receive_data.msg_iov = &vec;
            receive_data.msg_iovlen = 1;
            
            /// attempting to receive information
            int receive_status = recvmsg(newsockfd, &receive_data, MSG_WAITALL);
            if(receive_status == -1) {
                printf("error receiving data\n");
                continue;
            }
            // printf("%d\n", receive_status);

            /// connection with client open
            if(receive_status != 0) {
                if(buffer.type>4 || buffer.type<1)  /// wrong type
                    continue;
                /// preparing structure containing information about the client 
                client_data* cl_d = malloc(sizeof(client_data));
                cl_d->client_socket_fd = newsockfd;
                cl_d->qd = malloc(sizeof(query_data));
                cl_d->next = NULL;
                memcpy(cl_d->qd, &buffer, sizeof(query_data));

                /// adding client info to the end of the list, will be served by worker thread
                pthread_mutex_lock(&mt);

                client_data* tmp = td->cd;
                if(tmp == NULL) {
                    td->cd = cl_d;
                } else {
                    while(tmp->next != NULL) {
                        tmp = tmp->next;
                    }
                    tmp->next = cl_d;
                }
                pthread_mutex_unlock(&mt);
            }
        }


    }


    munmap(records, st.st_size);
    close(fd);
    free(servinfo);

    return;
}


int main(int argc, char** argv) {
    /// handling signal to shut down with closing file descriptors
    signal(SIGINT, ctrlc);

    /// no argument use default port
    if(argc == 1)
        start(PORT);
    else if(argc == 2)
        start(*(argv + 1));


    return 0;
}