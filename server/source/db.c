#include "../include/db.h"


void add_index(int** data, int* size, int* size_max, int value) {
    // printf("size%d/max%d\n", *size, *size_max);
    if((*size_max) == 0) {
        (*size_max) = 4;
        (*data) = calloc(*size_max, sizeof(int));
        (*data)[*size] = value;
        (*size) = *(size) + 1;
    } else if((*size)>=(*size_max)) {
        int* allocated = realloc(*data, (*size_max)*2*sizeof(int));
        if(allocated == NULL) {
            perror("bad realloc");
            exit(-1);
        }
        (*data) = allocated;
        (*size_max) = (*size_max)*2;
        (*data)[*size] = value;
        (*size) = *(size) + 1;
    } else {
        (*data)[*size] = value;
        (*size) = *(size) + 1;
    }
}

int search_id(record* rec, query_data* data) {
    char* end;
    int test = strtol(data->keyword, &end, 10);
    // printf("got %d\n", test);
    if (test > INT_MAX || (errno == ERANGE && test == LONG_MAX))
        return 0;
    if (test < INT_MIN || (errno == ERANGE && test == LONG_MIN))
        return 0;
    if (*end != '\0')
        return 0;
    // printf("%d %d\n", rec->client_id, test);
    if(test == rec->client_id) {
        return 1;
    }
    
    return 0;
}

int search_phone(record* rec, query_data* data) {
    char* end;
    int test = strtol(data->keyword, &end, 10);

    if (test > INT_MAX || (errno == ERANGE && test == LONG_MAX))
        return 0;
    if (test < INT_MIN || (errno == ERANGE && test == LONG_MIN))
        return 0;
    if (*end != '\0')
        return 0;

    if(test == rec->phone_nr)
        return 1;
    return 0;
}
int search_name(record* rec, query_data* data) {
    if(strstr(rec->name, data->keyword) != NULL)
        return 1;
    return 0;
}
int search_address(record* rec, query_data* data) {
    if(strstr(rec->address, data->keyword) != NULL)
        return 1;
    return 0;
}

struct iovec* db_lookup(record* rec, int db_size, query_data* data, int* size) {
    int* records_id = NULL;
    int records_n = 0;
    int rec_max = 0;

    searchtype fun;
    switch(data->type) {
        case 1:
            /// look for id
            fun = search_id;
            break;
        case 2:
            /// look for phone
            fun = search_phone;
            break;
        case 3:
            /// look for name
            fun = search_name;
            break;
        case 4:
            /// look for address
            fun = search_address;
            break;
        default:

            break;
    }

    /// if record found, index is added to the array
    for(int i=0; i<db_size; ++i) {
        if(fun(rec + i, data) == 1) {
            // printf("found %s\n", (rec + i)->name);
            add_index(&records_id, &records_n, &rec_max, i);
        }
    }

    /// amount of records rounded up to next batch size, recvmsg/sendmsg requires us to send unified amounts of data
    int data_sheet = (records_n/BATCHSIZE)*BATCHSIZE + BATCHSIZE;
    struct iovec* ret = calloc(data_sheet, sizeof(struct iovec));

    /// preparing iovec structure which later will be sent
    for(int i=0; i<records_n; ++i) {
        ret[i].iov_base = (rec + *(records_id + i));
        ret[i].iov_len = sizeof(record);
    }

    /// cleanup
    (*size) = data_sheet;
    free(records_id);
    return ret;
}
