#include "../include/thread_worker.h"

/// main thread serving requests
void* thread_worker(void* td) {

    thread_data* data = td;

    while(1) {

        /// checking if there is work to do
        pthread_mutex_lock(data->mtx);
        client_data* client = NULL;
        if(data->cd != NULL) {      /// popping data from the list
            client = data->cd;
            data->cd = data->cd->next;
            pthread_mutex_unlock(data->mtx);
        } else {                    /// nothing to do
            pthread_mutex_unlock(data->mtx);
            sched_yield();
            continue;
        }

        int size;
        /// looking up table, and getting data to send
        struct iovec* retdata = db_lookup(data->records, data->n_records, client->qd, &size);
        struct iovec* retdata_tmp = retdata;
        /// preparing message header structure
        struct msghdr message_data;
        bzero(&message_data, sizeof(message_data));
        message_data.msg_iov = retdata_tmp;
        message_data.msg_iovlen = BATCHSIZE;

        /// sending data in batches
        for(int i=0; i<(size/BATCHSIZE); ++i) {
            int send_status = sendmsg(client->client_socket_fd, &message_data, 0);
            if(send_status == -1) {
                printf("error sending data\n");
            }
            else {
            }
            message_data.msg_iov += BATCHSIZE;
        }
        printf("sent %d record(s) in %d message(s)\n", size, size/BATCHSIZE);
        close(client->client_socket_fd);
        free(retdata);
        free(client->qd);
    }
}