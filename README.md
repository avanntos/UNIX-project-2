# dbcrawler

Simple program which connects to multiple database servers and presents the user with the merged result of queries from all of them.
It was a project for Unix programming classes which aimed to utilize C programming language combined with UNIX system calls.
Dbcrawler has a config file with specified addresses of databases to connect to. For each address it will spawn a separate thread to connect to the database and receive the results using UNIX networking interface. After all results have been obtained the main thread will merge the results and show them to the user.

# TODO:
- improve how command line arguments work, right now.. it's not very polished.
- database server is very trivially implemented since it wasn't the main goal of this project, there is no query language for more complicated operations.

