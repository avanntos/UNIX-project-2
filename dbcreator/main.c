#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define DBSIZE 1000000

typedef struct record {
    int client_id;
    long phone_nr;
    char name[128];
    char address[128];
} record;

void fill(record* rec, int id, int phone_nr, const char* name, const char* address) {
    rec->client_id = id;
    rec->phone_nr = phone_nr;
    strncpy(rec->name, name, sizeof(rec->name));
    strncpy(rec->address, address, sizeof(rec->address));
}

char** getnames(const char* filename, int s) {
    FILE* f = fopen(filename, "r");
    char buffer[128];
    int i=0;
    char** ret = malloc(s*sizeof(char*));
    while(fscanf(f, "%s", buffer) && i < s) {
        *(ret + i) = malloc(strlen(buffer));
        strcpy(*(ret + i), buffer);
        ++i;
    }
    return ret;
}

int m_random(int min, int max) {
    return rand()%(max - min) + min;
}

int main() {
    srand(time(0));
    const char db_filename[] = "db1.txt";

    int fd = open(db_filename, O_CREAT | O_TRUNC | O_RDWR);
    if(fd < 0) {
        perror("cannot open file");
        exit(-1);
    }
    const int status = ftruncate(fd, sizeof(record)*DBSIZE);
    if(status != 0) {
        perror("truncate err");
        exit(-1);
    }
    record* records = mmap(0, sizeof(record)*DBSIZE, PROT_WRITE, MAP_SHARED, fd, 0);
    if(records == MAP_FAILED) {
        perror("mmap failed");
        exit(-1);
    }


    char** f = getnames("fem.txt", 80);
    char** m = getnames("men.txt", 80);
    char** c = getnames("cit.txt", 40);
    // for(int i=0; i<80; ++i) {
    //     printf("%s\n", *(f + i));
    // }

    for(int i=0; i<DBSIZE; ++i) {
        char** set = rand()%2 == 0 ? f : m;
        fill(records + i, i + 1, m_random(100000000, 999999999), *(set + m_random(0, 80)), *(c + m_random(0, 40)));
    }
    // for(int i=0; i<DBSIZE; ++i) {
    //     printf("%d, %ld, %s, %s\n", records[i].client_id, records[i].phone_nr, records[i].name, records[i].address);
    // }

    // fill(records + 0, 1, 123456789, "ryszard", "warszawa");
    // fill(records + 1, 2, 987654321, "ewelina", "krakow");
    // fill(records + 2, 3, 123789654, "krzysztof", "szczecin");
    // fill(records + 3, 4, 357682415, "justyna", "gdynia");
    // fill(records + 4, 5, 456321178, "jakub", "lodz");
    // fill(records + 5, 6, 753842694, "beata", "wroclaw");
    // fill(records + 6, 7, 456315897, "john", "london");
    // fill(records + 7, 8, 445684345, "dorota", "kielce");
    // fill(records + 8, 9, 995351351, "patryk", "lodz");
    // fill(records + 9, 10,123773210, "katarzyna", "zakopane");

    munmap(records, sizeof(record)*10);
    close(fd);
    return 0;
}